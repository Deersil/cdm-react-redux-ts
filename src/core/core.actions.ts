import { actionCreator } from '@/utils';

import * as CONSTANTS from './core.constants';

const action = actionCreator(CONSTANTS.MODULE_NAME);

export const checkAuth = action(CONSTANTS.CHECK_AUTH);
export const setAuthLoading = action(CONSTANTS.SET_AUTH_LOADER);